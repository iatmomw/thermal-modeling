/*****************************************************************************
 *                                McPAT
 *                      SOFTWARE LICENSE AGREEMENT
 *            Copyright 2012 Hewlett-Packard Development Company, L.P.
 *                          All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.”
 *
 ***************************************************************************/
#include "io.h"
#include <iostream>
#include "xmlParser.h"
#include "XML_Parse.h"
#include "processor.h"
#include "globalvar.h"
#include "version.h"
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <libgen.h>

#include "common_filename.h"
using namespace std;

//#define GPU_POWER
#define L2Cache

void print_usage(char * argv0);
void read_xmlfile(const char* directory,const char* fileprefix,std::vector<pair<int,string> > &filenames);
bool sortByCycle(const pair<int,string> &m,const pair<int,string> &n) { return m.first < n.first; }

//string mcpatXML;

int main(int argc,char *argv[])
{

	char * fb ;

	//mcpatXML="PowerData_MatMulUpdatedConfigV2.csv";

	bool infile_specified     = false;
	int  plevel               = 4;
	opt_for_clk	=true;
	//cout.precision(10);
	if (argc <= 1 || argv[1] == string("-h") || argv[1] == string("--help"))
	{
		print_usage(argv[0]);
	}

	for (int32_t i = 0; i < argc; i++)
	{
		if (argv[i] == string("-infile"))
		{
			infile_specified = true;
			i++;
			fb = argv[ i];
		}

		if (argv[i] == string("-print_level"))
		{
			i++;
			plevel = atoi(argv[i]);
		}

		if (argv[i] == string("-opt_for_clk"))
		{
			i++;
			opt_for_clk = (bool)atoi(argv[i]);
		}
	}

	if (infile_specified == false)
	{
		print_usage(argv[0]);
	}


	cout<<"McPAT (version "<< VER_MAJOR <<"."<< VER_MINOR
		<< " of " << VER_UPDATE << ") is computing the target processor...\n "<<endl;

	//cout<<"Please enter any key to enter into debug mode....."<<endl;
	//char debugc=getchar();

	//parsing XML file
	ParseXML *p1= new ParseXML();
	p1->parse(fb);
	//Processor proc(p1);
	//proc.displayEnergy(2, plevel);
#ifdef L2Cache

	char *path = dirname(fb);
	path = strcat(path,"/");

	std::vector<pair<int,string> > CoreFiles;
	std::vector<pair<int,string> > L2Files;
	std::vector<pair<int,string> > L1Files;
	std::vector<pair<int,string> > L1VFiles;

	read_xmlfile(path,"CycleLevelInformation",CoreFiles);
	read_xmlfile(path,"L2CacheCycle",L2Files);
	read_xmlfile(path,"L1CacheCycle",L1Files);
	read_xmlfile(path,"L1VectorCacheCycle",L1VFiles);

	L2TempStat l2caCacheStat;
	L1TempStat l1caCacheStat[8];
	L1VTempStat l1vcaCacheStat[32];
	dumpCUObjects cuObject[32];

	//initialization of cu stats
	for(int c=0;c<32;c++)
	{
		cuObject[c].committed_instructions=0;
		cuObject[c].total_instructions=0;
		cuObject[c].total_cycles=0;
		cuObject[c].busy_cycles=0;
		cuObject[c].int_regfile_reads=0;
		cuObject[c].scalar_instruction=0;
		cuObject[c].vector_instruction=0;
		cuObject[c].branch_instruction=0;
		cuObject[c].local_memoryAccess=0;
		cuObject[c].local_memoryRead=0;
		cuObject[c].local_memoryWrite=0;
		cuObject[c].local_memoryReadHit=0;
		cuObject[c].LDSIns[5]=0;
		cuObject[c].int_regfile_writes=0;
		cuObject[c].float_regfile_reads=0;
		cuObject[c].float_regfile_writes=0;

	}
	//initialization of cu stats
	for(int c=0;c<32;c++)
	{
		l1vcaCacheStat[c].read_access=0;
		l1vcaCacheStat[c].write_access=0;
		l1vcaCacheStat[c].read_miss=0;
		l1vcaCacheStat[c].write_miss=0;
	}


	//initialization of l1 stats
	int l=0;
	for(l=0;l<8;l++)
	{
		l1caCacheStat[l].read_access=0;
		l1caCacheStat[l].write_access=0;
		l1caCacheStat[l].read_miss=0;
		l1caCacheStat[l].write_miss=0;

	}
	//initialization of l2 stats
	l2caCacheStat.read_access=0;
	l2caCacheStat.write_access=0;
	l2caCacheStat.read_miss=0;
	l2caCacheStat.write_miss=0;

	int iterationCount=L2Files.size();
	//cout<<iterationCount<<endl;
	//cout<<"Working.."<<endl;
	for(int i=0;i<iterationCount;i++)
	{
		cout<<"Cycle "<<i+1<<" / "<<iterationCount<<endl;
		//print first cycle.
		//passing the input file one by one. As the values are incremental. Storing the value of current cycle's cu
		p1->gpuParse((path+CoreFiles[i].second).c_str(),cuObject,i);
		p1->gpuL2Parse((path+L2Files[i].second).c_str(),&l2caCacheStat,i);
		p1->gpuL1Parse((path+L1Files[i].second).c_str(),l1caCacheStat,i);
		p1->gpuL1VParse((path+L1VFiles[i].second).c_str(),l1vcaCacheStat,i);
		//p1->parse(fb);
		Processor proc(p1);
		//proc.displayEnergy(2, plevel);
		proc.dumpEnergyFile(2, plevel,true,L2Files[i].first);
		if(proc.l2.rt_power.readOp.dynamic<0)
		{
			cout<<"Power::"<<proc.l2.rt_power.readOp.dynamic<<endl;
			cout<<"Power::"<<proc.l2array[0]->rt_power.readOp.dynamic<<endl;
			break;
		}
	}
	delete p1;

	//cout<<"Done !!!"<<endl;
#endif

	return 0;
}

void print_usage(char * argv0)
{
    cerr << "How to use McPAT:" << endl;
    cerr << "  mcpat -infile <input file name>  -print_level < level of details 0~5 >  -opt_for_clk < 0 (optimize for ED^2P only)/1 (optimzed for target clock rate)>"<< endl;
    //cerr << "    Note:default print level is at processor level, please increase it to see the details" << endl;
    exit(1);
}

void read_xmlfile(const char* directory,const char* fileprefix,std::vector<pair<int,string> >&filenames)

{
	DIR* FD;
	struct dirent* in_file;

	/* Scanning the in directory */
	if ((FD = opendir (directory))==NULL) {
		printf("Error : Failed to open input directory - %s\n", strerror(errno));
		return;
	}

	int cycleNum=0;
	while ((in_file = readdir(FD)))	{
		/* On linux/Unix we don't want current and parent directories
		 * On windows machine too
		 */
		if (!strcmp (in_file->d_name, "."))
			continue;
		if (!strcmp (in_file->d_name, ".."))
			continue;

		/* Open directory entry file for common operation */
		string fullname=in_file->d_name;
		size_t pos1=fullname.find("_");
		size_t pos2=fullname.find(".");

		cycleNum=std::atoi(fullname.substr(pos1+1,pos2-pos1).c_str());
		if(fullname.substr(0,pos1)==fileprefix)
		{
			filenames.push_back(make_pair(cycleNum,fullname));
		}
	}

	sort(filenames.begin(),filenames.end(),sortByCycle);
}
