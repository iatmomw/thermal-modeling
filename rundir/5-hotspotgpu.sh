# Script to run hotspot gpu
# To simulate on "SIMID" subdirectory
# 	5-hotspotgpu.sh SIMID

## When no SIMID given, use default ID, "test"
if [ "$1" == "" ]; then
	SIMID=test
else
	SIMID=$1
fi

# Directory setting
Project_ROOT=/home/kchoo/workspace-thermalmodel/thermal-modeling
BENCHMARK=$Project_ROOT/benchmarks
M2S=$Project_ROOT/multi2sim-4.2
MCPATCPU=$Project_ROOT/mcpat12-cpu
MCPATGPU=$Project_ROOT/mcpat12-gpu
HOTSPOT=$Project_ROOT/HotSpot-6.0
PARSER=$Project_ROOT/reportparser
CONFIG=$Project_ROOT/configuration
RUNDIR=$Project_ROOT/rundir
# simulation specific
SIMDIR=$RUNDIR/$SIMID
RUNCONFIG=$SIMDIR/configuration
M2SResult=$SIMDIR/multi2sim-result
MCPATResult=$SIMDIR/mcpat-result
HOTSPOTResult=$SIMDIR/hotspot-result
######### SETTING UP TO THIS POINT ##############################################

# Extract dynamic power from the report
cd $PARSER
java McpatReportParserGPU $MCPATResult/gpu_report.txt > $MCPATResult/gpu.ptrace
cd $RUNDIR

# create directory when it does not exist
mkdir -p $HOTSPOTResult

$HOTSPOT/hotspot -c $RUNCONFIG/hotspot.config \
                 -f $RUNCONFIG/gpu32core.flp \
                 -p $MCPATResult/gpu.ptrace \
                 -o $HOTSPOTResult/gpu.ttrace \
                 -steady_file $HOTSPOTResult/gpu.steady

cp $HOTSPOTResult/gpu.steady $HOTSPOTResult/gpu.init

$HOTSPOT/hotspot -c $RUNCONFIG/hotspot.config \
                 -f $RUNCONFIG/gpu32core.flp \
                 -p $MCPATResult/gpu.ptrace \
                 -init_file $HOTSPOTResult/gpu.init \
                 -o $HOTSPOTResult/gpu.ttrace

$HOTSPOT/hotspot -c $RUNCONFIG/hotspot.config \
                 -f $RUNCONFIG/gpu32core.flp \
                 -p $MCPATResult/gpu.ptrace \
                 -steady_file $HOTSPOTResult/gpu.steady \
                 -model_type grid 

cp $HOTSPOTResult/gpu.steady $HOTSPOTResult/gpu.init

$HOTSPOT/hotspot -c $RUNCONFIG/hotspot.config \
                 -f $RUNCONFIG/gpu32core.flp \
                 -p $MCPATResult/gpu.ptrace \
                 -init_file $HOTSPOTResult/gpu.init \
                 -o $HOTSPOTResult/gpu.ttrace \
                 -model_type grid 

$HOTSPOT/hotspot -c $RUNCONFIG/hotspot.config \
                 -f $RUNCONFIG/gpu32core.flp \
                 -p $MCPATResult/gpu.ptrace \
                 -steady_file $HOTSPOTResult/gpu.steady \
                 -grid_steady_file $HOTSPOTResult/gpu.grid.steady \
                 -model_type grid


perl $HOTSPOT/grid_thermal_map.pl $RUNCONFIG/gpu32core.flp $HOTSPOTResult/gpu.grid.steady > $HOTSPOTResult/gpu.svg
convert -font Helvetica svg:$HOTSPOTResult/gpu.svg $HOTSPOTResult/gpu.pdf
