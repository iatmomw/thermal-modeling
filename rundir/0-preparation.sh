# Script to prepare simulation
# To create "SIMID" subfolder for output
# 	0-preparation.sh SIMID

## When no SIMID given, use default ID, "test"
if [ "$1" == "" ]; then
	SIMID=test
else
	SIMID=$1
fi

# Directory setting
Project_ROOT=/home/kchoo/workspace-thermalmodel/thermal-modeling
BENCHMARK=$Project_ROOT/benchmarks
M2S=$Project_ROOT/multi2sim-4.2
MCPATCPU=$Project_ROOT/mcpat12-cpu
MCPATGPU=$Project_ROOT/mcpat12-gpu
HOTSPOT=$Project_ROOT/HotSpot-6.0
PARSER=$Project_ROOT/reportparser
CONFIG=$Project_ROOT/configuration
RUNDIR=$Project_ROOT/rundir
# simulation specific
SIMDIR=$RUNDIR/$SIMID
RUNCONFIG=$SIMDIR/configuration
M2SResult=$SIMDIR/multi2sim-result
MCPATResult=$SIMDIR/mcpat-result
HOTSPOTResult=$SIMDIR/hotspot-result
######### SETTING UP TO THIS POINT ##############################################

mkdir -p $SIMDIR
# $1 is the simulation name
cp -rf $CONFIG $SIMDIR/
