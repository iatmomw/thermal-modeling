# Script to simulate on m2s
# To simulate on "SIMID" subdirectory
# 	1-run-multi2sim.sh SIMID

## When no SIMID given, use default ID, "test"
if [ "$1" == "" ]; then
	SIMID=test
else
	SIMID=$1
fi

# Directory setting
Project_ROOT=/home/kchoo/workspace-thermalmodel/thermal-modeling
BENCHMARK=$Project_ROOT/benchmarks
M2S=$Project_ROOT/multi2sim-4.2
MCPATCPU=$Project_ROOT/mcpat12-cpu
MCPATGPU=$Project_ROOT/mcpat12-gpu
HOTSPOT=$Project_ROOT/HotSpot-6.0
PARSER=$Project_ROOT/reportparser
CONFIG=$Project_ROOT/configuration
RUNDIR=$Project_ROOT/rundir
# simulation specific
SIMDIR=$RUNDIR/$SIMID
RUNCONFIG=$SIMDIR/configuration
M2SResult=$SIMDIR/multi2sim-result
MCPATResult=$SIMDIR/mcpat-result
HOTSPOTResult=$SIMDIR/hotspot-result

#Delete previous results
mkdir -p $M2SResult
mkdir -p $M2SResult/xmlreports-gpu
mkdir -p $M2SResult/xmlreports
#Delete previous results
\rm -rf $M2SResult/xmlreports-gpu/*
\rm -rf $M2SResult/xmlreports/*

cd $M2SResult
#$M2S/bin/m2s --x86-sim detailed --x86-config $RUNCONFIG/x86-config --si-sim detailed $BENCHMARK/SimpleConvolution/SimpleConvolution --load SimpleConvolution_Kernels.bin
#$M2S/bin/m2s --x86-sim detailed --x86-config $RUNCONFIG/x86-config --si-sim detailed $BENCHMARK/MatrixTranspose/MatrixTranspose --load MatrixTranspose_Kernels.bin
# CPU by forloop
#$M2S/bin/m2s --x86-sim detailed --si-sim detailed --mem-config $RUNCONFIG/mem-config --x86-config $RUNCONFIG/x86-config $BENCHMARK/VectorAddition/VectorAddition_forloopcpu
# CPU 
$M2S/bin/m2s --x86-sim detailed --si-sim detailed --mem-config $RUNCONFIG/mem-config --x86-config $RUNCONFIG/x86-config $BENCHMARK/VectorAddition/VectorAddition_CPU $BENCHMARK/VectorAddition/VectorAddition_KernelsCPU.bin
# GPU 
#$M2S/bin/m2s --x86-sim detailed --si-sim detailed --mem-config $RUNCONFIG/mem-config --x86-config $RUNCONFIG/x86-config $BENCHMARK/VectorAddition/VectorAddition_GPU $BENCHMARK/VectorAddition/VectorAddition_KernelsGPU.bin
cd $RUNDIR
