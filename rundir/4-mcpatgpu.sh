# Script to run mcpat gpu
# To simulate on "SIMID" subdirectory
# 	4-mcpatgpu.sh SIMID

## When no SIMID given, use default ID, "test"
if [ "$1" == "" ]; then
	SIMID=test
else
	SIMID=$1
fi

# Directory setting
Project_ROOT=/home/kchoo/workspace-thermalmodel/thermal-modeling
BENCHMARK=$Project_ROOT/benchmarks
M2S=$Project_ROOT/multi2sim-4.2
MCPATCPU=$Project_ROOT/mcpat12-cpu
MCPATGPU=$Project_ROOT/mcpat12-gpu
HOTSPOT=$Project_ROOT/HotSpot-6.0
PARSER=$Project_ROOT/reportparser
CONFIG=$Project_ROOT/configuration
RUNDIR=$Project_ROOT/rundir
# simulation specific
SIMDIR=$RUNDIR/$SIMID
RUNCONFIG=$SIMDIR/configuration
M2SResult=$SIMDIR/multi2sim-result
MCPATResult=$SIMDIR/mcpat-result
HOTSPOTResult=$SIMDIR/hotspot-result
######### SETTING UP TO THIS POINT ##############################################

# create directory when it does not exist
mkdir -p $MCPATResult
cp -f $RUNCONFIG/Xeon.xml $M2SResult/xmlreports-gpu

# Run mapat
$MCPATGPU/mcpat -infile $M2SResult/xmlreports-gpu/Xeon.xml > $MCPATResult/gpu_report.txt
