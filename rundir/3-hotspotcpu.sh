# Script to run hotspot cpu
# To simulate on "SIMID" subdirectory
# 	3-hotspotcpu.sh SIMID

## When no SIMID given, use default ID, "test"
if [ "$1" == "" ]; then
	SIMID=test
else
	SIMID=$1
fi

# Directory setting
Project_ROOT=/home/kchoo/workspace-thermalmodel/thermal-modeling
BENCHMARK=$Project_ROOT/benchmarks
M2S=$Project_ROOT/multi2sim-4.2
MCPATCPU=$Project_ROOT/mcpat12-cpu
MCPATGPU=$Project_ROOT/mcpat12-gpu
HOTSPOT=$Project_ROOT/HotSpot-6.0
PARSER=$Project_ROOT/reportparser
CONFIG=$Project_ROOT/configuration
RUNDIR=$Project_ROOT/rundir
# simulation specific
SIMDIR=$RUNDIR/$SIMID
RUNCONFIG=$SIMDIR/configuration
M2SResult=$SIMDIR/multi2sim-result
MCPATResult=$SIMDIR/mcpat-result
HOTSPOTResult=$SIMDIR/hotspot-result
######### SETTING UP TO THIS POINT ##############################################

# Extract dynamic power from the report
cd $PARSER
java McpatReportParserCPU $MCPATResult/cpu_report.txt > $MCPATResult/cpu.ptrace
cd $RUNDIR

# create directory when it does not exist
mkdir -p $HOTSPOTResult

$HOTSPOT/hotspot -c $RUNCONFIG/hotspot.config \
                 -f $RUNCONFIG/cpu4core.flp \
                 -p $MCPATResult/cpu.ptrace \
                 -steady_file $HOTSPOTResult/cpu.steady \
                 -grid_steady_file $HOTSPOTResult/cpu.grid.steady \
                 -model_type grid

perl $HOTSPOT/grid_thermal_map.pl $RUNCONFIG/cpu4core.flp $HOTSPOTResult/cpu.grid.steady > $HOTSPOTResult/cpu.svg
convert -font Helvetica svg:$HOTSPOTResult/cpu.svg $HOTSPOTResult/cpu.pdf
