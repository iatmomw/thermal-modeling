import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class McpatReportParserCPU {

	public static Scanner scan;
	public static ArrayList<Double> core0 = new ArrayList<Double>();
	public static ArrayList<Double> core1 = new ArrayList<Double>();
	public static ArrayList<Double> core2 = new ArrayList<Double>();
	public static ArrayList<Double> core3 = new ArrayList<Double>();
	public static ArrayList<Double> L20 = new ArrayList<Double>();
	public static ArrayList<Double> L21 = new ArrayList<Double>();
	public static ArrayList<Double> L22 = new ArrayList<Double>();
	public static ArrayList<Double> L23 = new ArrayList<Double>();
	public static ArrayList<Double> L3 = new ArrayList<Double>();
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		 
		scanInput(args[0]);
		print();
	}
	
	public static void print() throws FileNotFoundException, UnsupportedEncodingException{
		//PrintWriter writer = new PrintWriter("cpu.ptrace", "UTF-8");
		
		System.out.print("core0\t");
		System.out.print("core1\t");
		System.out.print("core2\t");
		System.out.print("core3\t");
		System.out.print("L20_cache\t");
		System.out.print("L21_cache\t");
		System.out.print("L22_cache\t");
		System.out.print("L23_cache\t");
		System.out.print("L3_cache\n");
		
		for(int i=0;i<L3.size() ;i++){
			
			System.out.print(core0.get(i)+"\t");
			System.out.print(core1.get(i)+"\t");
			System.out.print(core2.get(i)+"\t");
			System.out.print(core3.get(i)+"\t");
			System.out.print(L20.get(i)+"\t");
			System.out.print(L21.get(i)+"\t");
			System.out.print(L22.get(i)+"\t");
			System.out.print(L23.get(i)+"\t");
			System.out.print(L3.get(i));
			System.out.println();
		}
		//writer.close();
		
	}
	
	public static void scanInput(String arg) throws FileNotFoundException{
		scan = new Scanner(new File(arg));
		 
		while(scan.hasNextLine()){
			// Find Processor: line
			while(scan.hasNextLine() && !scan.nextLine().equals("Processor: "));

			String dummyStr = "";
			String [] token;
			
			// For each core
			for(int cores=0; cores<4; cores++){
				// Find Core: line
				while(scan.hasNextLine() && !scan.nextLine().equals("Core:"));
				// Find Runtime Dynamic line
				while(scan.hasNextLine()) {
					dummyStr = scan.nextLine();
					if (dummyStr.contains("Runtime Dynamic"))
						break;
				}
				
				token = (dummyStr.trim()).split(" ");
				if (token.length<=1)
					break;
				if (token[3].equals("inf")) {
					continue;
				} else if (cores==0){
					core0.add(Double.parseDouble(token[3]));
				} else if (cores==1){
					core1.add(Double.parseDouble(token[3]));
				} else if (cores==2){
					core2.add(Double.parseDouble(token[3]));
				} else if (cores==3){
					core3.add(Double.parseDouble(token[3]));
				}
				
				// Find L2 line
				while(scan.hasNextLine() && !scan.nextLine().contains("L2"));
				// Find Runtime Dynamic line
				while(scan.hasNextLine()) {
					dummyStr = scan.nextLine();
					if (dummyStr.contains("Runtime Dynamic"))
						break;
				}
				
				token = (dummyStr.trim()).split(" ");
				if (token.length<=1)
					break;
				if (token[3].equals("inf")) {
					continue;
				} else if (cores==0){
					L20.add(Double.parseDouble(token[3]));
				} else if (cores==1){
					L21.add(Double.parseDouble(token[3]));
				} else if (cores==2){
					L22.add(Double.parseDouble(token[3]));
				} else if (cores==3){
					L23.add(Double.parseDouble(token[3]));
				}
			}

			// Find L3 line				
			while(scan.hasNextLine() && !scan.nextLine().contains("L3"));
			// Find Runtime Dynamic line
			while(scan.hasNextLine()) {
				dummyStr = scan.nextLine();
				if (dummyStr.contains("Runtime Dynamic"))
					break;
			}
			token = (dummyStr.trim()).split(" ");
			if (token.length<=1)
				break;
			if (token[3].equals("inf")) {
				continue;
			} else {
				L3.add(Double.parseDouble(token[3]));
			}
		}
		//System.out.println(core0.toString());
		scan.close();	
	}
}