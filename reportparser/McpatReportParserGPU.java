import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class McpatReportParserGPU {

	public static Scanner scan;
	public static ArrayList<double[]> core = new ArrayList<double[]>();
	public static ArrayList<double[]> L1Scalar = new ArrayList<double[]>();
	public static ArrayList<double[]> L1Vector = new ArrayList<double[]>();
	public static ArrayList<double[]> L1Local = new ArrayList<double[]>();
	public static ArrayList<Double> L2 = new ArrayList<Double>();
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		
		scanInput(args[0]);
		print();
	}
	
	public static void print() throws FileNotFoundException, UnsupportedEncodingException{
		// Label
		for(int i=0; i<32; i++)
			System.out.print("Core"+(i+1)+"\t");
		for(int i=0; i<32; i++)
			System.out.print("L1V"+(i+1)+"\t");
		for(int i=0; i<32; i++)
			System.out.print("LMEM"+(i+1)+"\t");
		for(int i=0; i<8; i++)
			System.out.print("L1S"+(i+1)+"\t");
		System.out.println("L2_1");
		
		// Value
		for(int i=0; i<core.size(); i++){
			for(int j=0; j<32; j++)
				System.out.print(core.get(i)[j]+"\t");
			for(int j=0; j<32; j++)
				System.out.print(L1Vector.get(i)[j]+"\t");
			for(int j=0; j<32; j++)
				System.out.print(L1Local.get(i)[j]+"\t");
			for(int j=0; j<32; j+=4)
				System.out.print(L1Local.get(i)[j]+"\t");
			System.out.print(L2.get(i));
			System.out.println();
		}
	}
	
	public static void scanInput(String arg) throws FileNotFoundException{
		scan = new Scanner(new File(arg));
		
		while(scan.hasNextLine()){
			// Find Processor: line
			while(scan.hasNextLine() && !scan.nextLine().contains("Cycle: "));

			String dummyStr = "";
			String [] token;
			double [] CU  = new double[32];
			double [] L1V = new double[32];
			double [] L1S = new double[32];
			double [] L1L = new double[32];
			
			// For each core
			for(int cores=0; cores<32; cores++){
				// Find Core: line
				while(scan.hasNextLine() && !scan.nextLine().contains("Core"));
				// Find CU
				while(scan.hasNextLine()) { dummyStr = scan.nextLine(); if (dummyStr.contains("CU")) break;	}
				token = (dummyStr.trim()).split(" ");
				if (token.length<=1) break;
				if (token[3].equals("inf")) { continue;
				} else { CU[cores] = (Double.parseDouble(token[3])); }

				// Find L1Scalar
				while(scan.hasNextLine()) { dummyStr = scan.nextLine(); if (dummyStr.contains("L1Scalar")) break;	}
				token = (dummyStr.trim()).split(" ");
				if (token.length<=1) break;
				if (token[2].equals("inf")) { continue;
				} else { L1S[cores] = (Double.parseDouble(token[2])); }

				// Find L1Vector
				while(scan.hasNextLine()) { dummyStr = scan.nextLine(); if (dummyStr.contains("L1Vector")) break;	}
				token = (dummyStr.trim()).split(" ");
				if (token.length<=1) break;
				if (token[2].equals("inf")) { continue;
				} else { L1V[cores] = (Double.parseDouble(token[2])); }

				// Find Local memory
				while(scan.hasNextLine()) { dummyStr = scan.nextLine(); if (dummyStr.contains("Local memory")) break;	}
				token = (dummyStr.trim()).split(" ");
				if (token.length<=1) break;
				if (token[2].equals("inf")) { continue;
				} else { L1L[cores] = (Double.parseDouble(token[2])); }
			}
			
			core.add(CU);
			L1Scalar.add(L1S);
			L1Vector.add(L1V);
			L1Local.add(L1L);
					
			// Find L2 line
			while(scan.hasNextLine()) {
					dummyStr = scan.nextLine();
					if (dummyStr.contains("L2 cache"))
						break;
			}
				
			token = (dummyStr.trim()).split(" ");
			if (token.length<=1) break;
			if (token[2].equals("inf")) { continue;
			} else { L2.add(Double.parseDouble(token[2])); }
		}

		scan.close();	
	}
}