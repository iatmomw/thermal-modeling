#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
// OpenCL includes
#include <CL/cl.h>

#define CPU

// OpenCL kernel to perform an element-wise
// add of two arrays
int main(int argc, char * argv[])
{
		
	// This code executes on the OpenCL host
	// Host data
	int i = 0;
	int *A = NULL; // Input array
	int *B = NULL; // Input array
	int *C = NULL; // Output array

	// Elements in each array
	const int elements = 2048;
	// Compute the size of the data
	size_t datasize = sizeof(int)*elements;
	// Allocate space for input/output data
	A = (int*)malloc(datasize);
	B = (int*)malloc(datasize);
	C = (int*)malloc(datasize);
	
	// Initialize the input data

	for(i = 0; i < elements; i++) {
		A[i] = i;
		B[i] = i;
	}


	// Verify the output
	for(i = 0; i < elements; i++) {
		C[i] = A[i] + B[i];
		//if (temp!=C[i])
		//	result = false;
	}

	return 0;
}
